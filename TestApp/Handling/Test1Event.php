<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Handling;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class Test1Event
{
    public function __construct(
        public string $name = 'Test1Event'
    )
    {
    }
}

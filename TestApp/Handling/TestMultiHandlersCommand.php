<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Handling;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class TestMultiHandlersCommand
{
    public function __construct(
        public string $name = 'TestMultiHandlersCommand'
    )
    {
    }
}

<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\HandlingExternal;

use GDXbsv\PServiceBus\Message\ExternalOut;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
#[ExternalOut('memory-external', 'test.external_out_event')]
final class ExternalOutEvent
{
    public function __construct(
        public string $name = 'ExternalOutEvent'
    ) {
    }
}

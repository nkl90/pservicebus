<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\HandlingExternal;

use GDXbsv\PServiceBus\Bus\Handling\Handle;
use GDXbsv\PServiceBus\Message\MessageHandlerContext;

/**
 * @internal
 */
final class HandlersExternal
{
    public string $result = '';

    #[Handle('memory')]
    public function handleExternal1(ExternalOutEvent $event, MessageHandlerContext $context): void
    {
        $this->result .= '||' . $event->name;
    }
}

<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\HandlingExternal;

use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBus\Transport\Transport;

class InMemoryExternalTransport implements Transport
{
    /** @var list<Envelope> */
    public array $envelopes = [];

    /**
     * @return \Generator<int, void, Envelope|null, void>
     * @psalm-suppress MixedReturnTypeCoercion Still do not know why
     */
    public function sending(): \Generator
    {
        while (true) {
            $envelope = (yield);
            if (!$envelope) {
                break;
            }
            $this->envelopes[] = $envelope;
        }
    }

    public function receive(int $limit = 0): \Generator
    {
        if (count($this->envelopes) === 0) {
            return;
        }
        $envelopes = $this->envelopes;
        $this->envelopes = [];
        foreach ($envelopes as $envelope) {
            $result = yield $envelope;
            /** @psalm-suppress UnusedMethodCall */
            $result->unwrap();
        }
    }

    public function stop(): void
    {
        // we can not stop or we will lose all the messages.
    }
}

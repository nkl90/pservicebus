<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

use Doctrine\Instantiator\Instantiator;
use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\Message\MessageOptions;
use GDXbsv\PServiceBus\Saga\SagaFind;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class CustomSagaFinder
{
    #[SagaFind]
    public function findByMultipleFields(
        CustomSearchEvent $event,
        MessageOptions $messageOptions
    ): TestSaga {
        // DO some fancy search in SQL for example

        /** @var TestSaga $instance */
        $instance = (new Instantiator())->instantiate(TestSaga::class);
        $hydrate = \Closure::bind(
            function (TestSaga $object): void {
                $object->id = new Id('customSagaId');
            },
            null,
            TestSaga::class
        );
        /** @psalm-suppress PossiblyInvalidFunctionCall */
        $hydrate($instance);
        return $instance;
    }
}

<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

use Doctrine\ORM\Mapping as ORM;
use GDXbsv\PServiceBus\Bus\Handling\Handle;
use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\Message\TimeSpan;
use GDXbsv\PServiceBus\Saga\MessageSagaContext;
use GDXbsv\PServiceBus\Saga\Saga;
use GDXbsv\PServiceBus\Saga\SagaCreateMapper;
use GDXbsv\PServiceBus\Saga\SagaPropertyMapper;


/**
 * @ORM\Entity()
 * @final
 */
final class TestSaga extends Saga
{
    /**
     * @ORM\Column(type="id", nullable=false)
     * @ORM\Id
     */
    private Id $id;
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    public string $string;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public ?string $value;

    /**
     * @param Id<static> $id
     */
    private function __construct(Id $id, string $string)
    {
        $this->id = $id;
        $this->string = $string;
    }

    public static function configureHowToCreateSaga(SagaCreateMapper $mapper): void
    {
        $mapper
            ->toMessage(
                // do not forget to create handling function in a case if saga exists and to let saga know that we wait this message
                function (TestSagaCreateCommand $command, MessageSagaContext $context) {
                    return new self(new Id($command->id), $command->string);
                }
            );
    }

    public static function configureHowToFindSaga(SagaPropertyMapper $mapper): void
    {
        $mapper
            ->mapSaga(new \ReflectionProperty(TestSaga::class, 'id'))
            ->toMessage(
                function (TestSagaCommand $command, MessageSagaContext $context) {
                    return new Id($command->id);
                }
            )
            ->toMessage(
                function (TestsSagaInEvent $message, MessageSagaContext $context) {
                    return new Id($message->string);
                }
            )
            ->toMessage(
                function (TestsSagaInTimeoutEvent $message, MessageSagaContext $context) {
                    return new Id($message->string);
                }
            )
            ->toMessage(
                function (TestsSagaOutputTimeoutEvent $message, MessageSagaContext $context) {
                    return new Id($message->string);
                }
            )
            ->toMessage(
                function (TestSagaRemoveCommand $command, MessageSagaContext $context) {
                    return new Id($command->id);
                }
            );
        $mapper
            ->mapSaga(new \ReflectionProperty(TestSaga::class, 'string'))
            ->toMessage(
                function (TestSagaMapStringCommand $command, MessageSagaContext $context) {
                    return $command->string;
                }
            );
    }

    /** We have to tell saga we wait this message */
    #[Handle('memory', 3)]
    public function testSagaCreateCommand(TestSagaCreateCommand $command, MessageSagaContext $context)
    {
        $this->string = $command->string;
    }

    #[Handle('memory', 3)]
    public function testHandlerFunction(TestSagaCommand $command, MessageSagaContext $context)
    {
        $this->string = $command->string;
        $this->timeout(new TestsSagaOutputEvent('testHandlerFunction'), TimeSpan::fromSeconds(0));
    }

    #[Handle('memory', 3)]
    public function testRemove(TestSagaRemoveCommand $command, MessageSagaContext $context)
    {
        $this->markAsComplete();
    }

    #[Handle('memory', 3)]
    public function testListeningFunction(TestsSagaInEvent $event, MessageSagaContext $context)
    {
        $this->string = $event->string;
        $this->value = $event->value;
        $this->apply(new TestsSagaOutputEvent('testListeningFunction'));
    }

    #[Handle('memory', 3)]
    public function testListeningWithTimeoutFunction(TestsSagaInTimeoutEvent $event, MessageSagaContext $context)
    {
        $this->string = $event->string;
        $this->value = $event->value;
        $this->timeout(new TestsSagaOutputTimeoutEvent('testListeningWithTimeoutFunction'), TimeSpan::fromSeconds(3));
    }

    #[Handle('memory', 3)]
    public function testListeningAfterTimeoutFunction(
        TestsSagaOutputTimeoutEvent $event,
        MessageSagaContext $context
    ) {
        $this->apply(new TestsSagaOutputEvent('testListeningAfterTimeoutFunction'));
    }

    #[Handle('memory', 3)]
    public function testListeningCustomSearchEvent(
        CustomSearchEvent $event,
        MessageSagaContext $context
    ) {
        $this->apply(new TestsSagaOutputEvent($this->id->toString()));
    }

    #[Handle('memory', 3)]
    public function testListeningCustomDoctrienSearchEvent(
        CustomDoctrineSearchEvent $event,
        MessageSagaContext $context
    ) {
        $this->apply(new TestsSagaOutputEvent($this->string));
    }

    #[Handle('memory', 3)]
    public function handleTestSagaMapStringCommand(
        TestSagaMapStringCommand $command,
        MessageSagaContext $context
    ) {
        $this->apply(new TestsSagaOutputEvent($this->id->toString()));
    }
}

<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

class TestSagaCommand
{
    public string $id = 'testSaga';
    public string $string = 'testSagaString';
}

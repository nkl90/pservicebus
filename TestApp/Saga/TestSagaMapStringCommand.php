<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

class TestSagaMapStringCommand
{
    public string $string = 'testSaga';
}

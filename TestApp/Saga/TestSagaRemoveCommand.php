<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

class TestSagaRemoveCommand
{
    public string $id = 'testSaga';
    public string $string = 'testSagaString';
}

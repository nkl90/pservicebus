<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

final class TestsSagaInEvent
{
    public ?string $string = 'testSaga';
    public ?string $value = 'secretValue';
}

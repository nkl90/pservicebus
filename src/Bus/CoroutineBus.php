<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus;

use GDXbsv\PServiceBus\Message\CommandOptions;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;

/**
 * @template T as object
 */
interface CoroutineBus
{
    /**
     * @return \Generator<int, bool, Message<CommandOptions>|null, void>
     */
    public function sendCoroutine(): \Generator;
    /**
     * @return \Generator<int, bool, Message<EventOptions>|null, void>
     */
    public function publishCoroutine(): \Generator;
}

<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling;

use GDXbsv\PServiceBus\Message\TimeSpan;

/**
 * @immutable
 * @psalm-immutable
 */
#[\Attribute(\Attribute::TARGET_FUNCTION | \Attribute::TARGET_METHOD)]
final class Handle
{
    /**
     * @param non-empty-string $transportName
     */
    public function __construct(
        /** @var non-empty-string */
        public string $transportName,
        public ?int $retries = null,
        public ?int $timeoutSec = null,
    ) {
    }
}

<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Bus\Handling\MessageHandleContext;
use GDXbsv\PServiceBus\Bus\Handling\MessageHandleInstruction;
use GDXbsv\PServiceBus\Bus\Middleware\InMiddleware;
use GDXbsv\PServiceBus\Bus\Middleware\OutMiddleware;
use GDXbsv\PServiceBus\Message\CommandOptions;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Message\MessageOptions;
use GDXbsv\PServiceBus\Saga\Saga;
use GDXbsv\PServiceBus\Saga\SagaHandling;
use GDXbsv\PServiceBus\Serializer\Serializer;
use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBus\Transport\Transport;
use Prewk\Result\Err;
use Prewk\Result\Ok;

/**
 * @psalm-import-type MessageClassMap from \GDXbsv\PServiceBus\Message\ScrapeExternals
 * @psalm-import-type MessageNameMap from \GDXbsv\PServiceBus\Message\ScrapeExternals
 */
final class ServiceBus implements Bus, ConsumeBus, CoroutineBus
{
    private bool $shouldStop = false;
    /** @var \Closure(\Throwable, Message<MessageOptions>):void */
    private \Closure $errorHandler;
    /** @var list<OutMiddleware> */
    private array $outMiddlewares = [];
    /** @var list<InMiddleware> */
    private array $inMiddlewares = [];

    /**
     * @param array<string, Transport> $transportMap
     * @param array<class-string, list<MessageHandleInstruction>> $handlingInstructions
     * @param array<class-string, object> $handlerToObjectMap
     * @param MessageClassMap $messageInClassMap
     * @param MessageClassMap $messageOutClassMap
     * @param MessageNameMap $messageNameMap
     */
    public function __construct(
        private SagaHandling $sagaHandling,
        private Serializer $serializer,
        private array $transportMap,
        private array $handlingInstructions,
        private array $handlerToObjectMap,
        private array $messageInClassMap,
        private array $messageOutClassMap,
        private array $messageNameMap,
    ) {
        $this->errorHandler = $this->errorHandlerDefault();

        $intersect = array_intersect(array_keys($this->messageInClassMap), array_keys($this->messageOutClassMap));
        if (count($intersect) > 0) {
            $intersect = join(', ', $intersect);
            throw new \Exception("You have intersections between In and Out events: ({$intersect})");
        }
    }

    public function send(object $message, CommandOptions|null $commandOptions = null): void
    {
        if (!$commandOptions) {
            $commandOptions = CommandOptions::record();
        }
        $coroutine = $this->sendCoroutine();
        $coroutine->send(new Message($message, $commandOptions));
        $coroutine->send(null);
    }

    public function publish(object $message, EventOptions|null $eventOptions = null): void
    {
        if (!$eventOptions) {
            $eventOptions = EventOptions::record();
        }
        $coroutine = $this->publishCoroutine();
        $coroutine->send(new Message($message, $eventOptions));
        $coroutine->send(null);
    }

    /**
     * @psalm-suppress LessSpecificReturnStatement yes we know that we want to use specific message inside
     * @psalm-suppress MoreSpecificReturnType yes we know that we want to use specific message inside
     */
    public function sendCoroutine(): \Generator
    {
        return $this->coroutine();
    }

    /**
     * @psalm-suppress LessSpecificReturnStatement yes we know that we want to use specific message inside
     * @psalm-suppress MoreSpecificReturnType yes we know that we want to use specific message inside
     */
    public function publishCoroutine(): \Generator
    {
        return $this->coroutine();
    }

    /**
     * @return \Generator<int, bool, Message<MessageOptions>|null, void>
     */
    private function coroutine(): \Generator
    {
        /** @var array<string, \Generator<int, void, Envelope|null, void>> $sendings */
        $sendings = [];
        while (true) {
            $message = (yield true);
            if ($message === null) {
                foreach ($sendings as $sending) {
                    $sending->send(null);
                }


                return;
            }
            foreach ($this->outMiddlewares as $middleware) {
                $middleware->before($message);
            }
            $payload = $message->payload;
            $messageType = $payload::class;
            if (!$this->checkCommandHasOneHandler($message)) {
                throw new \RuntimeException(
                    "Command '{$messageType}' must have exactly one handler. But we have: "
                    . (string)count($this->handlingInstructions[$messageType] ?? [])
                );
            }
            foreach ($this->messageEnvelopes($message) as [$envelope, $transport]) {
                if (!array_key_exists($transport::class, $sendings)) {
                    $sendings[$transport::class] = $transport->sending();
                }
                $sending = $sendings[$transport::class];
                $sending->send($envelope);
            }
            foreach ($this->outMiddlewares as $middleware) {
                $middleware->after();
            }
        }
    }

    public function consume(Transport $transport, int $limit = 0): \Traversable
    {
        $envelopesGenerator = $transport->receive($limit);

        while ($envelopesGenerator->valid()) {
            try {
                /** @psalm-suppress PossiblyNullArgument according to type it not null */
                $message = $this->envelopeForHandling($envelopesGenerator->current());
                foreach ($this->inMiddlewares as $middleware) {
                    $middleware->before($message);
                }
                /** @var ?string $handlerName */
                $handlerName = $message->options->headers['handlerName'] ?? null;
                assert(isset($message->options->headers['type']));
                /** @var non-empty-string $type */
                $type = $message->options->headers['type'];
                if ($type === 'external') {
                    $this->redriveExternalInInternal($message);
                } elseif ($type === 'saga') {
                    $this->sagaHandling->handle($message);
                } else {
                    $object = $this->handlerToObjectMap[$handlerName] ?? null;
                    $methodName = $message->options->headers['handlerMethodName'] ?? null;
                    /** @var bool $isStaticMethod */
                    $isStaticMethod = $message->options->headers['isStaticMethod'] ?? false;
                    assert(is_string($methodName));
                    if ($isStaticMethod && $handlerName) {
                        /** @psalm-suppress InvalidStringClass */
                        $handlerName::{$methodName}(
                            $message->payload,
                            new MessageHandleContext($message->options, $this, $this)
                        );
                    } elseif ($object) {
                        /** @psalm-suppress MixedMethodCall */
                        $object->{$methodName}(
                            $message->payload,
                            new MessageHandleContext($message->options, $this, $this)
                        );
                    } else {
                        $methodName($message->payload, new MessageHandleContext($message->options, $this, $this));
                    }
                }
                $envelopesGenerator->send(new Ok(null));
                foreach ($this->inMiddlewares as $middleware) {
                    $middleware->after(new Ok(null));
                }
                yield $message;
            } catch (\Throwable $throwable) {
                $result = new Err(new \Exception('Consume: ' . $throwable->getMessage(), 0, $throwable));
                $envelopesGenerator->send($result);
                if (!isset($message)) {
                    $message = new Message(new \stdClass(), MessageOptions::record());
                }
                ($this->errorHandler)($throwable, $message);
            }

            if ($this->shouldStop) {
                $transport->stop();
            }
        }
    }

    public function stop(): void
    {
        $this->shouldStop = true;
    }

    /**
     * @param \Closure(\Throwable, Message):void $errorHandler
     */
    public function setErrorHandler(\Closure $errorHandler): self
    {
        $this->errorHandler = $errorHandler;

        return $this;
    }

    /**
     * @return \Closure(\Throwable, Message):void
     */
    private function errorHandlerDefault(): \Closure
    {
        return static function (\Throwable $_t, Message $_message): void {
            throw $_t;
        };
    }

    public function addInMiddleware(InMiddleware $middleware): void
    {
        $this->inMiddlewares[] = $middleware;
    }

    public function addOutMiddleware(OutMiddleware $middleware): void
    {
        $this->outMiddlewares[] = $middleware;
    }

    /**
     * @return \Traversable<array{Envelope, Transport}>
     */
    private function messageEnvelopes(Message $message): \Traversable
    {
        $payload = $message->payload;
        $messageType = $payload::class;
        $options = $message->options;

        //region REPLAY
        if (isset($options->headers['replay_type']) && $options->headers['replay_type'] === 'external') {
            $options = $options->withHeader('replay_type', 'replayed');
            goto external;
        }
        if (isset($options->headers['transportName'], $options->headers['handlerName'], $options->headers['handlerMethodName'],)) {
            assert(is_string($options->headers['transportName']));
            assert(is_string($options->headers['handlerName']));
            if (is_subclass_of($options->headers['handlerName'] ?: '', Saga::class)) {
                $options = $options
                    ->withHeader('saga', $options->headers['handlerName'])
                    ->withHeader('type', 'saga');
            } else {
                $options = $options->withHeader('type', $options::class);
            }
            yield [
                new Envelope(
                    $this->serializer->serialize($payload),
                    $options->retries,
                    $options->timeout->intervalSec,
                    $options
                        ->withHeader('name', $messageType)
                        ->toMap()
                ),
                $this->transportMap[$options->headers['transportName']]
            ];

            return;
        }
        //endregion


        //region INTERNAL
        if (isset($this->handlingInstructions[$messageType])) {
            foreach ($this->handlingInstructions[$messageType] as $instruction) {
                if ($instruction->timeout) {
                    $options = $options->withTimeout($instruction->timeout);
                }
                if ($instruction->retries) {
                    $options = $options->withRetries($instruction->retries);
                }
                if (is_subclass_of($instruction->handlerName ?: '', Saga::class)) {
                    $options = $options
                        ->withHeader('saga', $instruction->handlerName)
                        ->withHeader('type', 'saga');
                } else {
                    $options = $options->withHeader('type', $options::class);
                }
                yield [
                    new Envelope(
                        $this->serializer->serialize($payload),
                        $options->retries,
                        $options->timeout->intervalSec,
                        $options
                            ->withHeader('name', $messageType)
                            ->withHeader('handlerName', $instruction->handlerName)
                            ->withHeader('handlerMethodName', $instruction->handlerMethodName)
                            ->withHeader('isStaticMethod', $instruction->isStaticMethod)
                            ->withHeader('transportName', $instruction->transportName)
                            ->toMap()
                    ),
                    $this->transportMap[$instruction->transportName]
                ];
            }
        }
        //endregion

        //region EXTERNAL
        external:
        if (isset($this->messageOutClassMap[$messageType])) {
            [$transportName, $messageName] = $this->messageOutClassMap[$messageType];
            if (isset($this->messageInClassMap[$messageType])) {
                throw new \RuntimeException('You can send only ExternalOut messages to external.');
            }
            $options = $options
                ->withHeader('name', $messageName)
                ->withHeader('type', 'external');
            if (!isset($options->headers['transportName'])) {
                $options = $options
                    ->withHeader('transportName', $transportName);
            }
            yield [
                new Envelope(
                    $this->serializer->serialize($payload),
                    $options->retries,
                    $options->timeout->intervalSec,
                    $options->toMap()
                ),
                $this->transportMap[$transportName]
            ];
        }
        //endregion
    }

    private function envelopeForHandling(Envelope $envelope): Message
    {
        $payloadRaw = $envelope->payload;

        /** @var array{name:string, type:string, saga: string} $headers */
        $headers = $envelope->headers;
        $payloadFqn = $headers['name'];
        if ($headers['type'] === 'external') {
            if (!isset($this->messageNameMap[$payloadFqn])) {
               throw new \RuntimeException("No type defined: '{$payloadFqn}'");
            }
            $payloadFqn = $this->messageNameMap[$payloadFqn];
        }

        return new Message(
            $this->serializer->deserialize(
                $payloadRaw,
                $payloadFqn
            ),
            MessageOptions::fromMap($envelope->headers)
        );
    }

    private function checkCommandHasOneHandler(Message $message): bool
    {
        if (!($message->options instanceof CommandOptions)) {
            return true;
        }
        if (count($this->handlingInstructions[$message->payload::class] ?? []) === 1) {
            return true;
        }
        return false;
    }

    private function redriveExternalInInternal(Message $message): void
    {
        $eventOptions = EventOptions::record($message->options->headers)
            ->withHeader('external', true)
            ->withHeader('external_from_id', $message->options->messageId->toString());
        $this->publish($message->payload, $eventOptions);
    }
}


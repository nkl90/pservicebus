<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Message\CommandOptions;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Transport\Transport;

final class TraceableBus implements Bus, ConsumeBus, CoroutineBus, Bus\Middleware\OutMiddleware
{
    /** @var Message[] */
    private array $sent = [];
    /** @var Message[] */
    private array $published = [];
    private bool $tracing = false;

    public function __construct(private ServiceBus $bus)
    {
        $this->bus->addOutMiddleware($this);
    }

    public function before(Message $message): void
    {
        if (!$this->tracing) {
            return;
        }

        if ($message->options instanceof CommandOptions) {
            $this->sent[] = $message;
        } elseif ($message->options instanceof EventOptions) {
            $this->published[] = $message;
        }
    }

    public function after(): void
    {
    }

    /**
     * @return Message[]
     */
    public function getEventMessages(): array
    {
        return $this->published;
    }

    /**
     * @param class-string $className
     */
    public function getEventMessageByClass(string $className): Message
    {
        foreach ($this->published as $message) {
            if ($message->payload::class === $className) {
                return $message;
            }
        }

        throw new \Exception("Did not find message with class '$className'");
    }

    /**
     * @return Message[]
     */
    public function getCommandMessages(): array
    {
        return $this->sent;
    }

    /**
     * @param class-string $className
     */
    public function getCommandMessageByClass(string $className): Message
    {
        foreach ($this->sent as $message) {
            if ($message->payload::class === $className) {
                return $message;
            }
        }

        throw new \Exception("Did not find message with class '$className'");
    }

    /**
     * Start tracing.
     */
    public function trace(): void
    {
        $this->tracing = true;
    }

    public function send(object $message, ?CommandOptions $commandOptions = null): void
    {
        $this->bus->send($message, $commandOptions);
    }

    public function publish(object $message, ?EventOptions $eventOptions = null): void
    {
        $this->bus->publish($message, $eventOptions);
    }

    /**
     * @return \Generator<int, bool, Message<CommandOptions>|null, void>
     */
    public function sendCoroutine(): \Generator
    {
        return $this->bus->sendCoroutine();
    }

    /**
     * @return \Generator<int, bool, Message<EventOptions>|null, void>
     */
    public function publishCoroutine(): \Generator
    {
        return $this->bus->publishCoroutine();
    }

    public function consume(Transport $transport, int $limit = 0): \Traversable
    {
        return $this->bus->consume($transport, $limit);
    }

    public function stop(): void
    {
        $this->bus->stop();
    }
}

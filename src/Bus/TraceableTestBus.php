<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Message\CommandOptions;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;

final class TraceableTestBus implements Bus, CoroutineBus
{
    /** @var Message[] */
    private array $sent = [];
    /** @var Message[] */
    private array $published = [];


    /**
     * @return Message[]
     */
    public function getEventMessages(): array
    {
        return $this->published;
    }

    /**
     * @param class-string $className
     */
    public function getEventMessageByClass(string $className): Message
    {
        foreach ($this->published as $message) {
            if ($message->payload::class === $className) {
                return $message;
            }
        }

        throw new \Exception("Did not find message with class '$className'");
    }

    /**
     * @return Message[]
     */
    public function getCommandMessages(): array
    {
        return $this->sent;
    }

    /**
     * @param class-string $className
     */
    public function getCommandMessageByClass(string $className): Message
    {
        foreach ($this->sent as $message) {
            if ($message->payload::class === $className) {
                return $message;
            }
        }

        throw new \Exception("Did not find message with class '$className'");
    }

    public function send(object $message, ?CommandOptions $commandOptions = null): void
    {
        $this->sent[] = new Message($message, $commandOptions ?? CommandOptions::record());
    }

    public function publish(object $message, ?EventOptions $eventOptions = null): void
    {
        $this->published[] = new Message($message, $eventOptions ?? EventOptions::record());
    }

    /**
     * @return \Generator<int, bool, Message<CommandOptions>|null, void>
     */
    public function sendCoroutine(): \Generator
    {
        $sent = true;
        while (true) {
            $message = (yield $sent);
            if ($message === null) {
                return;
            }
            $this->sent[] = $message;
        }
    }

    /**
     * @return \Generator<int, bool, Message<EventOptions>|null, void>
     */
    public function publishCoroutine(): \Generator
    {
        $sent = true;
        while (true) {
            $message = (yield $sent);
            if ($message === null) {
                return;
            }
            $this->published[] = $message;
        }
    }
}

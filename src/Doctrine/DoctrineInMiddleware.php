<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use GDXbsv\PServiceBus\Bus\Middleware\InMiddleware;
use GDXbsv\PServiceBus\Message\Message;
use Prewk\Result;

class DoctrineInMiddleware implements InMiddleware
{
    public function __construct(
        private EntityManagerInterface $em,
        private ManagerRegistry $registry,
    )
    {
    }

    public function before(Message $message): void
    {
        if (!$this->em->isOpen()) {
            $this->registry->resetManager();
        }
    }

    public function after(Result $result): void
    {
        $this->em->flush();
        $this->em->clear();
    }
}

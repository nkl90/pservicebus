<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use GDXbsv\PServiceBus\Id;
use Ramsey\Uuid\Uuid;

final class IdUuidBinType extends Type
{
    const NAME = 'id_bin';

    /**
     * @param string|null $value
     * @psalm-suppress MoreSpecificImplementedParamType It is okay to have more specific type
     * @return Id|null
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value === '') {
            return null;
        }

        return new Id(Uuid::fromBytes($value)->toString());
    }

    /**
     * {@inheritdoc}
     *
     * @param Id|null|'' $value
     * @psalm-suppress MoreSpecificImplementedParamType It is okay to have more specific type
     *
     * @return string|null
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value === '') {
            return null;
        }

        return Uuid::fromString($value->toString())->getBytes();
    }

    public function getName()
    {
        return self::NAME;
    }

    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform)
    {
        return $platform->getBinaryTypeDeclarationSQL(
            [
                'length' => '16',
                'fixed' => true,
            ]
        );
    }
}

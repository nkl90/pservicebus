<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\GuidType;
use GDXbsv\PServiceBus\Id;

final class IdUuidType extends GuidType
{
    private const NAME = 'id_uuid';

    /**
     * @param string|null|Id $value
     * @psalm-suppress MoreSpecificImplementedParamType It is okay to have more specific type
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value === '') {
            return null;
        }

        if ($value instanceof Id) {
            return $value;
        }

        return new Id($value);
    }

    /**
     * {@inheritdoc}
     *
     * @param Id|null          $value
     * @psalm-suppress MoreSpecificImplementedParamType It is okay to have more specific type
     *
     * @return string|null
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        return $value->toString();
    }

    public function getName()
    {
        return self::NAME;
    }
}

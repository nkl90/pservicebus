<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus;

use Ramsey\Uuid\Uuid as RamseyUuid;

/**
 * @immutable
 * @psalm-immutable
 * @template CLASS as object
 */
class Id
{
    final public static function new(): self
    {
        return new self(RamseyUuid::uuid6()->toString());
    }

    /**
     * @psalm-param non-empty-string $value
     */
    final public function __construct(private string $value)
    {
    }

    /**
     * @psalm-return non-empty-string
     */
    final public function toString(): string
    {
        return $this->value;
    }

    /**
     * @psalm-return non-empty-string
     */
    final public function __toString(): string
    {
        return $this->toString();
    }
}

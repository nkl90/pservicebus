<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus;

/**
 * @template CLASS as object
 * @immutable
 * @psalm-immutable
 */
class IdCollection
{
    /**
     * @param list<Id<CLASS>> $ids
     */
    final private function __construct(public array $ids)
    {
    }

    /**
     * @template CLASS_FUNC as object
     * @param list<Id<CLASS_FUNC>> $ids
     * @return IdCollection<CLASS_FUNC>
     */
    final public static function ofObjects(array $ids): IdCollection
    {
        return new self($ids);
    }

    /**
     * @template CLASS_FUNC as object
     * @param \Traversable<Id<CLASS_FUNC>> $ids
     * @return IdCollection<CLASS_FUNC>
     */
    final public static function ofIterator(\Traversable $ids): IdCollection
    {
        /** @psalm-suppress MixedArgumentTypeCoercion does not detect iterator_to_array correctly */
        return new self(iterator_to_array($ids));
    }

    /**
     * @param array<non-empty-string> $ids
     * @return IdCollection<CLASS>
     * @psalm-suppress InvalidReturnStatement
     * @psalm-suppress InvalidReturnType
     */
    final public static function ofStrings(array $ids): IdCollection
    {
        $idObjects = [];
        foreach ($ids as $stringId) {
            $idObjects[] = new Id($stringId);
        }

        return new self($idObjects);
    }

    /**
     * @param Id<CLASS> $id
     * @return static<CLASS>
     */
    final public function add(Id $id): self
    {
        return new self([...$this->ids, $id]);
    }
}

<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message;

/**
 * @immutable
 * @psalm-immutable
 * @template T as MessageOptions
 */
final class Message
{
    /**
     * @param T $options
     */
    public function __construct(
        public object $payload,
        public MessageOptions $options
    ) {
    }
}

<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message;

/**
 * @immutable
 * @psalm-immutable
 * @deprecated please use MessageHandleContext or MessageSagaContext
 */
class MessageHandlerContext
{
    public function __construct(
        public MessageOptions $messageOptions
    ) {
    }
}

<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message;

/**

 * @immutable
 * @psalm-immutable
 * @psalm-type MessageClassMap=array<class-string, array{non-empty-string,non-empty-string}>
 * @psalm-type MessageNameMap=array<non-empty-string, class-string>
 */
final class ScrapeExternals
{
    /**
     * @param list<class-string> $classNames
     * @param class-string<ExternalOut>|class-string<ExternalIn> $attributeClassName
     * @return array{MessageClassMap, MessageNameMap}
     */
    public static function fromClasses(array $classNames, string $attributeClassName): array
    {
        $messageClassMap = [];
        $messageNameMap = [];

        foreach ($classNames as $className) {
            $reflectionClass = new \ReflectionClass($className);
            $attributes = $reflectionClass->getAttributes($attributeClassName);
            if (count($attributes) === 0) {
                continue;
            }
            foreach ($attributes as $attribute) {
                /** @var ExternalOut|ExternalIn $externalAttr */
                $externalAttr = $attribute->newInstance();

                $messageClassMap[$className] = [$externalAttr->transportName, $externalAttr->externalName];
                $messageNameMap[$externalAttr->externalName] = $className;
            }
        }

        return [$messageClassMap, $messageNameMap];
    }
}

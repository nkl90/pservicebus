<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message;

/**
 * @immutable
 * @psalm-immutable
 */
final class TimeSpan
{
    private function __construct(public int $intervalSec)
    {
    }

    public static function fromSeconds(int $seconds): self
    {
        return new self($seconds);
    }

    public static function fromMinutes(int $minutes): self
    {
        return new self(60 * $minutes);
    }
}

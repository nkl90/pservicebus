<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

/**
 * @immutable
 * @psalm-immutable
 */
abstract class CorrelationSagaToMessageMap extends SagaToMessageMap
{
    /** @var \ReflectionProperty */
    protected \ReflectionProperty $sagaProperty;
    /** @var \Closure(object):\GDXbsv\PServiceBus\Id<Saga> $messageProperty */
    protected \Closure $messageProperty;
}

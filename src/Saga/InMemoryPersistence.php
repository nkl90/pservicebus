<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\Message\Message;

/**
 * @psalm-import-type FindersMap from \GDXbsv\PServiceBus\Saga\ScrapeFinders
 */
final class InMemoryPersistence implements SagaPersistence
{
    private CoroutineBus $coroutineBus;
    /** @var list<Saga> */
    private array $sagaList = [];

    /**
     * @param FindersMap $findInstructions
     * @param array<non-empty-string, object> $findToObjectMap
     */
    public function __construct(private SagaFindDefinitions $sagaFinding, private array $findInstructions, private array $findToObjectMap)
    {
    }

    public function setCoroutineBus(CoroutineBus $coroutineBus): void
    {
        $this->coroutineBus = $coroutineBus;
    }

    public function retrieveSaga(Message $message, string $sagaType): ?Saga
    {
        $payload = $message->payload;
        if (isset($this->findInstructions[$sagaType][$payload::class])) {
            $findInstruction = $this->findInstructions[$sagaType][$payload::class];
            assert(isset($this->findToObjectMap[$findInstruction->className]));
            /**
             * @var Saga $saga
             * @psalm-suppress MixedMethodCall
             */
            $saga = $this->findToObjectMap[$findInstruction->className]->{$findInstruction->methodName}(
                $payload,
                $message->options
            );
            return $saga;
        }

        foreach ($this->sagaFinding->sagasForMessage($payload) as $finderDefinition) {
            if ($finderDefinition->sagaType !== $sagaType) {
                continue;
            }
            $propertyValue = ($finderDefinition->messageProperty)($payload, new MessageSagaContext($message->options));

            /** @psalm-suppress MixedArgument */
            $saga = $this->findSagaByProperty($finderDefinition->sagaProperty->name, $propertyValue);
            if ($saga) {
                return $saga;
            }
        }

        foreach ($this->sagaFinding->sagaCreatorsForMessage($payload) as $creatorDefinition) {
            if ($creatorDefinition->sagaType !== $sagaType) {
                continue;
            }
            return ($creatorDefinition->sagaCreator)($payload, new MessageSagaContext($message->options));
        }

        return null;
    }

    public function saveSaga(Saga $saga): void
    {
        $coroutine = $this->coroutineBus->publishCoroutine();
        foreach ($saga->getUncommittedMessages() as $message) {
            $coroutine->send($message);
        }
        $coroutine->send(null);
        $this->sagaList[] = $saga;
    }

    public function cleanSaga(Saga $saga): void
    {
    }

    /**
     * @param mixed $propertyValue
     */
    protected function findSagaByProperty(string $propertyName, $propertyValue): ?Saga
    {
        foreach ($this->sagaList as $saga) {
            [$val,] = $this->getSagaInfo($saga, $propertyName);
            /** @psalm-suppress MixedMethodCall */
            if (is_object($propertyValue) && $propertyValue->toString() === $val->toString()) {
                return $saga;
            }
            if ($propertyValue === $val) {
                return $saga;
            }
        }
        return null;
    }

    /**
     * @return array{Id<Saga>, positive-int}
     * @psalm-suppress MixedInferredReturnType
     */
    protected function getSagaInfo(Saga $saga, string $propertyName): array
    {
        /** @var \Closure():array{scalar, positive-int} $closure */
        $closure = function () use ($propertyName): array {
            /**
             * @var Saga $this
             * @psalm-suppress InaccessibleProperty
             */
            return [$this->$propertyName, $this->_playhead];
        };
        /** @psalm-suppress MixedReturnStatement */
        return $closure->call($saga);
    }
}

<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Message\MessageHandlerContext;
use GDXbsv\PServiceBus\Message\MessageOptions;

/**
 * @psalm-suppress DeprecatedClass Will remove it in the next version
 * @psalm-suppress MissingImmutableAnnotation Will remove it in the next version
 */
final class MessageSagaContext extends MessageHandlerContext
{
    public function __construct(
        public MessageOptions $messageOptions,
    ) {
    }
}

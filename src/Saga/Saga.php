<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Message\TimeSpan;

abstract class Saga
{
    /** @var list<Message<EventOptions>> */
    private array $uncommittedMessages = [];
    /** @var int */
    protected int $_playhead = 0;
    /**
     * @psalm-readonly-allow-private-mutation
     */
    public ?\DateTimeImmutable $completedAt = null;

    abstract public static function configureHowToCreateSaga(SagaCreateMapper $mapper): void;

    abstract public static function configureHowToFindSaga(SagaPropertyMapper $mapper): void;

    /**
     * @throws \Exception
     */
    protected function apply(object $event): void
    {
        ++$this->_playhead;
        /** @psalm-suppress InvalidPropertyAssignmentValue psalm does not detect correct type */
        $this->uncommittedMessages[] = new Message(
            $event,
            EventOptions::record()->withHeader('playhead', $this->_playhead)
        );
    }

    /**
     * @throws \Exception
     */
    protected function timeout(object $event, TimeSpan $timeSpan): void
    {
        ++$this->_playhead;
        /** @psalm-suppress InvalidPropertyAssignmentValue psalm does not detect correct type */
        $this->uncommittedMessages[] = new Message(
            $event,
            EventOptions::record()->withHeader(
                'playhead',
                $this->_playhead
            )->withTimeout($timeSpan)
        );
    }

    protected function markAsComplete(): void
    {
        $this->completedAt = new \DateTimeImmutable();
    }

    /**
     * @return list<Message<EventOptions>>
     */
    public function getUncommittedMessages(): array
    {
        $collected = $this->uncommittedMessages;
        $this->uncommittedMessages = [];

        return $collected;
    }
}

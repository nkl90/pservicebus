<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

final class SagaCreateMapper
{
    /**
     * @param class-string<Saga> $sagaType
     */
    public function __construct(
        private string $sagaType,
        private SagaCreateByMessageConfiguration $sagaCreateByMessageConfiguration
    ) {
    }

    /**
     * @param \Closure(object, MessageSagaContext):?Saga $sagaCreator
     */
    public function toMessage(\Closure $sagaCreator): self
    {
        $this->sagaCreateByMessageConfiguration->configureCreateMapping($this->sagaType, $sagaCreator);

        return $this;
    }
}

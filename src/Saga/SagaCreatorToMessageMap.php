<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

/**
 * @immutable
 * @psalm-immutable
 */
final class SagaCreatorToMessageMap extends SagaCreateToMessageMap
{
    /** @var \Closure(object, MessageSagaContext):?Saga  */
    private \Closure $sagaCreator;

    /**
     * @param class-string<Saga> $sagaType
     * @param \Closure(object, MessageSagaContext):?Saga $sagaCreator
     */
    public function __construct(string $sagaType, \Closure $sagaCreator)
    {
        $this->sagaType = $sagaType;
        $this->sagaCreator = $sagaCreator;
    }

    public function createSagaCreatorDefinition(): SagaCreatorDefinition
    {
        return new SagaCreatorDefinition($this->sagaType, $this->sagaCreator);
    }
}

<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

#[\Attribute(\Attribute::TARGET_FUNCTION | \Attribute::TARGET_METHOD)]
final class SagaFind
{
}

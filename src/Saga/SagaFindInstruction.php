<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Message\TimeSpan;

/**
 * @immutable
 * @psalm-immutable
 */
final class SagaFindInstruction
{
    public function __construct(
        public string $className,
        public string $methodName,
    ) {
    }
}

<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

final class SagaFinderDefinition
{
    /**
     * @param class-string<Saga> $sagaType
     * @param \Closure(object, \GDXbsv\PServiceBus\Saga\MessageSagaContext):\GDXbsv\PServiceBus\Id<Saga> $messageProperty
     */
    public function __construct(
        public string $sagaType,
        public \ReflectionProperty $sagaProperty,
        public \Closure $messageProperty
    ) {
    }
}

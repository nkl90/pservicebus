<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Message\Message;

final class SagaHandling
{
    public function __construct(private SagaPersistence $sagaPersistence,)
    {
    }

    public function handle(Message $message): void
    {
        try {
            assert(isset($message->options->headers['saga']));
            /** @var class-string<Saga> $sagaType */
            $sagaType = $message->options->headers['saga'];
            $saga = $this->sagaPersistence->retrieveSaga($message, $sagaType);
            if (!$saga) {
                return;
            }
            $this->execute($message, $saga);
            $this->sagaPersistence->saveSaga($saga);
        } catch (\Throwable $throwable) {
            if (isset($saga)) {
                $this->sagaPersistence->cleanSaga($saga);
            }
            throw $throwable;
        }
    }

    protected function execute(Message $message, Saga $saga): void
    {
        $messagePayload = $message->payload;
        $sagaReflection = new \ReflectionClass($saga);
        foreach (
            $sagaReflection->getMethods(
                \ReflectionMethod::IS_STATIC | \ReflectionMethod::IS_PUBLIC
            ) as $reflectionMethod
        ) {
            $parameters = $reflectionMethod->getParameters();
            if (count($parameters) !== 2) {
                continue;
            }
            [$messageParameter, ,] = $parameters;
            /** @var \ReflectionNamedType|null $messageType */
            $messageType = $messageParameter->getType();
            if ($messageType && $messageType->getName() !== $messagePayload::class) {
                continue;
            }
            $reflectionMethod->invokeArgs(
                $saga,
                [
                    $messagePayload,
                    new MessageSagaContext($message->options),
                ]
            );
        }
    }
}

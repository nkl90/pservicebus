<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Message\MessageOptions;

/**
 * @immutable
 * @psalm-immutable
 * @psalm-type FindersMap=array<class-string<Saga>, array<class-string, SagaFindInstruction>>
 */
final class ScrapeFinders
{
    /**
     * @param list<class-string> $classNames
     * @return FindersMap
     */
    public static function fromClasses(array $classNames): array
    {
        $finders = [];

        foreach ($classNames as $className) {
            $reflectionClass = new \ReflectionClass($className);
            foreach ($reflectionClass->getMethods() as $method) {
                $attributes = $method->getAttributes(SagaFind::class);
                if (count($attributes) === 0) {
                    continue;
                }
                $arguments = $method->getParameters();
                assert(count($arguments) === 2);
                /** @var \ReflectionNamedType $type */
                $type = $arguments[0]->getType();
                /** @var class-string $messageType */
                $messageType = $type->getName();
                /** @var \ReflectionNamedType $type2 */
                $type2 = $arguments[1]->getType();
                assert($type2->getName() === MessageOptions::class);
                /**
                 * @var class-string<Saga> $sagaType
                 * @psalm-suppress UndefinedMethod
                 * @psalm-suppress PossiblyNullReference
                 */
                $sagaType = $method->getReturnType()->getName();
                $finders[$sagaType][$messageType] = new SagaFindInstruction(
                    $className,
                    $method->getName(),
                );
            }
        }

        return $finders;
    }
}

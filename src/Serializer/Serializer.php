<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Serializer;

interface Serializer
{
    public function serialize(object $data): array;

    public function deserialize(array $serializedData, string $class): object;
}

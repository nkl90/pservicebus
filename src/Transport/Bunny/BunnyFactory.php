<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Bunny;

use Bunny\Async\Client;
use Bunny\Channel;
use React\EventLoop\Factory;

/**
 * @internal
 */
final class BunnyFactory
{
    public static function getChannel(array $bunnyOptions): array
    {
        $loop = Factory::create();
        $channel = (new Client($loop, $bunnyOptions))->connect()
            ->then(
                fn(Client $client) => $client->channel()
            )
            ->then(
                function (Channel $channel) {
                    return $channel->qos(0, 5)->then(
                        function () use ($channel) {
                            return $channel;
                        }
                    );
                }
            )
            ->then(
                function (Channel $channel) {
                    return $channel->confirmSelect()->then(
                        function () use ($channel) {
                            return $channel;
                        }
                    );
                }
            );

        return [$channel, $loop];
    }
}

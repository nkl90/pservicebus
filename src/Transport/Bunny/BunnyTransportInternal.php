<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Bunny;

use Bunny\Channel;
use Bunny\Message;
use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBus\Transport\Transport;
use GDXbsv\PServiceBus\Transport\TransportSynchronisation;
use Prewk\Result;
use React\EventLoop\LoopInterface;
use React\Promise\PromiseInterface;

use function Clue\React\Block\await;
use function Clue\React\Block\awaitAll;
use function React\Promise\all;
use function React\Promise\resolve;

/**
 * @internal
 */
class BunnyTransportInternal implements Transport, TransportSynchronisation
{
    protected const PART_ONE = 0;
    protected const PART_TWO = 1;
    protected const ACCUMULATE = 30;

    protected bool $shouldStop = false;
    protected string $queueNameDlq;
    protected string $queueNameTimeout;

    public function __construct(
        private string $queueName,
        protected PromiseInterface $channel,
        protected LoopInterface $loop
    ) {
        $this->queueNameDlq = $queueName . '_DL';
        $this->queueNameTimeout = $queueName . '_TO';
    }

    public function sending(): \Generator
    {
        await($this->channel, $this->loop);
        $envelope = (yield);
        if ($envelope === null) {
            return;
        }
        $promises = [
            self::PART_ONE => [],
            self::PART_TWO => [],
        ];
        $partCurrent = self::PART_ONE;
        while ($envelope) {
            $promises[$partCurrent][] = $this->sendEnvelope($envelope);
            /** @psalm-suppress RedundantCondition */
            if ($partCurrent === self::PART_ONE && count($promises[self::PART_ONE]) >= self::ACCUMULATE) {
                $partCurrent = self::PART_TWO;
                if (count($promises[self::PART_TWO]) >= self::ACCUMULATE) {
                    awaitAll($promises[self::PART_TWO], $this->loop);
                    $promises[self::PART_TWO] = [];
                }
            }
            if ($partCurrent === self::PART_TWO && count($promises[self::PART_TWO]) >= self::ACCUMULATE) {
                $partCurrent = self::PART_ONE;
                if (count($promises[self::PART_ONE]) >= self::ACCUMULATE) {
                    awaitAll($promises[self::PART_ONE], $this->loop);
                    $promises[self::PART_ONE] = [];
                }
            }
            $envelope = (yield);
        }

        awaitAll(\array_merge($promises[self::PART_TWO], $promises[self::PART_ONE]), $this->loop);

        $promises[self::PART_TWO] = [];
        $promises[self::PART_ONE] = [];
    }

    public function receive(int $limit = 0): \Generator
    {
        await($this->channel, $this->loop);
        $messagesLeft = $limit;
        $envelopePromise = $this->receiveMessage();
        $finishPromise = resolve(true);
        while (true) {
            /** @var Message|null $bunnyMessage */
            $bunnyMessage = await($envelopePromise, $this->loop);
            $messagesLeft = ($messagesLeft === 0) ? $messagesLeft : $messagesLeft - ($bunnyMessage ? 1 : 0);
            if ($limit === 0 && !$this->shouldStop && $bunnyMessage === null) {
                $this->waitForContinue(1);
            }
            if (($limit === 0 && !$this->shouldStop) || ($messagesLeft > 0 && $bunnyMessage !== null && !$this->shouldStop)) {
                $envelopePromise = $this->receiveMessage();
            } else {
                $envelopePromise = null;
            }

            if ($bunnyMessage !== null) {
                try {
                    $envelope = BunnyEnvelope::ofBunnyMessage($bunnyMessage)->toEnvelope();
                } catch (\Throwable $t) {
                    await($this->nack($bunnyMessage), $this->loop);
                    throw $t;
                }
                /** @var Result $isSucceed */
                $isSucceed = (yield $envelope);
            }

            await($finishPromise, $this->loop);
            if ($bunnyMessage !== null) {
                if ($isSucceed->isErr()) {
                    $finishPromise = $this->extendVisibilityTimeout($bunnyMessage);
                } else {
                    $finishPromise = $this->asc($bunnyMessage);
                }
            }

            if ($envelopePromise === null) {
                break;
            }
        }
    }

    public function stop(): void
    {
        $this->shouldStop = true;
    }

    public function sync(): void
    {
        await(
            $this->channel->then(
                function (Channel $channel) {
                    $promises = [];
                    $promises[] = $channel->queueDeclare($this->queueNameDlq);
                    $promises[] = $channel->queueDeclare(
                        $this->queueName,
                        arguments: [
                            "x-dead-letter-exchange" => '',
                            "x-dead-letter-routing-key" => $this->queueNameDlq
                        ]
                    );
                    $promises[] = $channel->queueDeclare(
                        $this->queueNameTimeout,
                        arguments: [
                            "x-dead-letter-exchange" => '',
                            "x-dead-letter-routing-key" => $this->queueName
                        ]
                    );

                    return all($promises);
                }
            ),
            $this->loop
        );
    }

    protected function sendEnvelope(Envelope $envelope): PromiseInterface
    {
        $queueName = $this->queueName;
        if ($envelope->timeoutSec > 0) {
            $queueName = $this->queueNameTimeout;
        }

        return $this->channel
            ->then(
                function (Channel $channel) use ($envelope, $queueName) {
                    [$body, $headers] = BunnyEnvelope::ofEnvelope($envelope)->toBunnyPublish();
                    return $channel->publish($body, $headers, routingKey: $queueName);
                }
            );
    }

    private function receiveMessage(): PromiseInterface
    {
        return $this->channel
            ->then(
                function (Channel $channel) {
                    return $channel->get($this->queueName);
                }
            )
            ->then(
                function (?Message $message) {
                    return $message;
                }
            );
    }

    private function asc(Message $message): PromiseInterface
    {
        return $this->channel
            ->then(
                function (Channel $channel) use ($message) {
                    return $channel->ack($message);
                }
            );
    }

    private function nack(Message $message): PromiseInterface
    {
        return $this->channel
            ->then(
                function (Channel $channel) use ($message) {
                    return $channel->nack($message, requeue: false);
                }
            );
    }

    private function extendVisibilityTimeout(Message $message): PromiseInterface
    {
        $h = $message->headers;
        $h['psb-retried'] = $h['psb-retried'] + 1;
        if (!isset($h['retries']) || ($h['retries'] = 0) || ($h['psb-retried'] > $h['retries'])) {
            return $this->channel
                ->then(
                    function (Channel $channel) use ($message) {
                        // send To dlq
                        return $channel->nack($message, requeue: false);
                    }
                );
        }

        $h['expiration'] = 60 * (2 ** $h['psb-retried']) * 1000; //in millisecond
        return $this->channel
            ->then(
                function (Channel $channel) use ($message, $h) {
                    return $channel->publish(
                        $message->content,
                        $h,
                        routingKey: $this->queueNameTimeout
                    )->then(
                        function () use ($channel) {
                            return $channel;
                        }
                    );
                }
            )
            ->then(
                function (Channel $channel) use ($message) {
                    // remove
                    return $channel->ack($message);
                }
            );
    }

    private function waitForContinue(int $seconds): void
    {
        for ($i = 0; $i < $seconds; ++$i) {
            sleep(1);
            if ($this->shouldStop) {
                return;
            }
        }
    }
}

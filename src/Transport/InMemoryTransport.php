<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport;

use GDXbsv\PServiceBus\Bus\ConsumeBus;
use Prewk\Result;

class InMemoryTransport implements Transport
{
    /** @var list<Envelope> */
    private array $envelopes = [];
    private ?ConsumeBus $bus;
    private bool $isDispatching = false;

    /**
     * @return \Generator<int, void, Envelope|null, void>
     * @psalm-suppress MixedReturnTypeCoercion Still do not know why
     */
    public function sending(): \Generator
    {
        while (true) {
            $envelope = (yield);
            if (!$envelope) {
                break;
            }
            $this->envelopes[] = $envelope;
        }
        assert(isset($this->bus));
        if (!$this->isDispatching) {
            $this->isDispatching = true;
            try {
                foreach ($this->bus->consume($this) as $_message) {
                };
            } finally {
                $this->isDispatching = false;
            }
        }

    }

    /**
     * @return \Generator<int, Envelope, Result\Ok<null, mixed>|Result\Err<mixed, \Exception>, void>
     */
    public function receive(int $limit = 0): \Generator
    {
        if (count($this->envelopes) === 0) {
            return;
        }
        while ($envelope = array_shift($this->envelopes)) {
            $result = yield $envelope;
            $result->unwrap();
        }
    }

    public function stop(): void
    {
        // we can not stop or we will lose all the messages.
    }

    public function setBus(ConsumeBus $bus): void
    {
        $this->bus = $bus;
    }
}

<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Sns;

use GDXbsv\PServiceBus\Transport\Envelope;

/**
 * @internal
 */
final class SnsEnvelope
{
    /**
     * @param array<string, string|int|bool|null> $headers
     */
    public function __construct(
        private string $payload,
        private int $retries,
        private int $timeoutSec,
        /** @var array<string, string|int|bool|null> */
        private array $headers,
    ) {
    }

    public static function ofEnvelope(Envelope $envelope): self
    {
        return new self(
            \json_encode($envelope->payload, JSON_THROW_ON_ERROR),
            $envelope->retries,
            $envelope->timeoutSec,
            $envelope->headers
        );
    }
    public static function ofSqsRaw(array $messageRaw): self
    {
        assert(isset($messageRaw['Body']));
        assert(is_string($messageRaw['Body']));
        /** @var array{Type:string, Message: string, MessageId:string, TopicArn:string, Timestamp:string, MessageAttributes: array<string,array{Value: string, Type: string}>} $message */
        $message = json_decode($messageRaw['Body'], true, 512, JSON_THROW_ON_ERROR);
        $attributes = self::generateFromAttributes($message['MessageAttributes']);
        assert(isset($attributes['headers']) && is_string($attributes['headers']));
        /** @var array<string, bool|int|null|string> $headers */
        $headers = json_decode($attributes['headers'], true, 512, JSON_THROW_ON_ERROR);
        /** @var int|string $reties */
        $reties = $headers['retries'] ?? 0;
        /** @var int|string $timeoutSec */
        $timeoutSec = $headers['timeout_sec'] ?? 0;
        return new self(
            $message['Message'],
            (int)$reties,
            (int)$timeoutSec,
            $headers
        );
    }

    public function toSnsArray(string $topicArn): array
    {
        assert(isset($this->headers['name']));
        $messageAttributes = $this->generateAttributes(
            [
                'headers' => json_encode($this->headers, JSON_THROW_ON_ERROR),
                'name' => $this->headers['name'],
            ]
        );

        $publish = [
            'Message' => $this->payload,
            'TopicArn' => $topicArn,
        ];

        if (0 !== count($messageAttributes)) {
            $publish['MessageAttributes'] = $messageAttributes;
        }

        return $publish;
    }

    public function toEnvelope(): Envelope
    {
        /** @psalm-suppress MixedArgument we control what we receive. It must be an array */
        return new Envelope(\json_decode($this->payload, true, 512, JSON_THROW_ON_ERROR), $this->retries, $this->timeoutSec, $this->headers);
    }

    /**
     * @param iterable<string,string|int|bool|null> $attributes
     */
    private function generateAttributes(iterable $attributes): array
    {
        $messageAttributes = [];
        foreach ($attributes as $attributeName => $attributeValue) {
            $dataType = 'String';
            if (is_numeric($attributeValue)) {
                $dataType = 'Number';
            }
            $messageAttributes[$attributeName] = [
                'DataType'    => $dataType,
                'StringValue' => $attributeValue,
            ];
        }

        return $messageAttributes;
    }

    /**
     * @param iterable<string,array{Value: string, Type: string}> $messageAttributes
     * @return array<string, bool|int|null|string>
     */
    private static function generateFromAttributes(iterable $messageAttributes): array
    {
        $attributes = [];
        foreach ($messageAttributes as $attributeName => ['Value' => $val, 'Type' => $type]) {
            $attributes[$attributeName] = match ($type) {
                'String' => $val,
                'Number' => (int)$val,
            };
        }

        return $attributes;
    }
}

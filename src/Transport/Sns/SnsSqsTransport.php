<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Sns;

use Aws\Credentials\CredentialProvider;
use Aws\Sns\SnsClient;
use Enqueue\Dsn\Dsn;
use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBus\Transport\Sqs\SqsTransport;
use GDXbsv\PServiceBus\Transport\Transport;
use GDXbsv\PServiceBus\Transport\TransportSynchronisation;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\Utils;
use Prewk\Result;

/**
 * Now support only one queue for all messages.
 * You can modify this transport for multi sqs queues and multi topics support.
 * For example topic+sqs for each message, ot subscription+sqs for each message
 *
 * @psalm-import-type MessageNameMap from \GDXbsv\PServiceBus\Message\ScrapeExternals
 * @psalm-suppress    InternalProperty
 * @psalm-suppress    InternalClass
 * @psalm-suppress    InternalMethod
 */
class SnsSqsTransport implements TransportSynchronisation, Transport
{
    private const PART_ONE = 0;
    private const PART_TWO = 1;

    private bool $shouldStop = false;

    public function __construct(
        private SnsClient $snsClient,
        private string $topicArn,
        private SqsTransport $sqsTransport,
        /** @var MessageNameMap */
        private array $messageNameMapIn,
    ) {
        $this->sqsTransport->setAllowFrom($this->topicArn);
    }

    /**
     * @param MessageNameMap $messageNameMapIn
     */
    public static function ofDsn(
        string $dsnString,
        SqsTransport $sqsTransport,
        /** @var MessageNameMap */
        array $messageNameMapIn,
        array $clientAdditionalConfig = []
    ): self {
        $dsn = Dsn::parseFirst($dsnString);
        if (null === $dsn || $dsn->getSchemeProtocol() !== 'sns') {
            throw new \InvalidArgumentException(
                'Malformed parameter "dsn". example: "sns+http://key:secret@aws:4100/123456789012?region=eu-west-1&topic=testTopic"'
            );
        }

        $namespace = $dsn->getPath();
        assert(
            $namespace !== null,
            'error(-><-): sns+http://key:secret@aws:4100->/123456789012<-?region=eu-west-1&topic=testTopic'
        );
        $topic = $dsn->getString('topic');
        assert(
            $topic !== null,
            'error(-><-): sns+http://key:secret@aws:4100/123456789012?region=eu-west-1&->topic=testTopic<-'
        );
        $region = $dsn->getString('region');
        assert(
            $region !== null,
            'error(-><-): sns+http://key:secret@aws:4100/123456789012?->region=eu-west-1<-&topic=testTopic'
        );
        $user = $dsn->getUser();
        $password = $dsn->getPassword();

        $topicArn = 'arn:aws:sns:' . $region . ':' . trim($namespace, '/') . ':' . $topic;

        $snsClientConfig = [
            'version' => 'latest',
            'region' => $region,
        ];
        if ($user !== null && $password !== null) {
            $snsClientConfig['credentials'] = [
                'key' => $user,
                'secret' => $password,
            ];
        } else {
            $provider = CredentialProvider::defaultProvider();
            $snsClientConfig['credentials'] = $provider;
        }
        $host = $dsn->getHost();
        if ($host !== null) {
            /**
             * @psalm-suppress PossiblyUndefinedIntArrayOffset
             * @var string $schemeExtension
             */
            $schemeExtension = $dsn->getSchemeExtensions()[0];
            $endpoint = sprintf(
                '%s://%s',
                $schemeExtension,
                $host
            );
            if ($dsn->getPort() !== null) {
                $endpoint .= ":{$dsn->getPort()}";
            }
            $snsClientConfig['endpoint'] = $endpoint;
        }

        return new self(
            new SnsClient(array_merge($snsClientConfig, $clientAdditionalConfig)),
            $topicArn,
            $sqsTransport,
            $messageNameMapIn
        );
    }

    /**
     * @return \Generator<int, Envelope, Result\Ok<null, mixed>|Result\Err<mixed, \Exception>, void>
     */
    public function receive(int $limit = 0): \Generator
    {
        $envelopesGenerator = $this->sqsTransport->receiveRaw($limit);
        while ($envelopesGenerator->valid()) {
            $messageRaw = $envelopesGenerator->current();
            /** @psalm-suppress PossiblyNullArgument according to type it not null */
            $result = yield SnsEnvelope::ofSqsRaw($messageRaw)->toEnvelope();

            $envelopesGenerator->send($result);

            if ($this->shouldStop) {
                $this->sqsTransport->stop();
            }
        }
    }

    public function stop(): void
    {
        $this->shouldStop = true;
        $this->sqsTransport->stop();
    }

    public function sync(): void
    {
        $this->sqsTransport->sync();
        $arn = $this->sqsTransport->arn();
        // mostly need for tests (gtilab-ci problems)
        $parts = explode(':', $arn);
        $parts[2] = 'sqs';
        $arn = implode(':', $parts);
        if (!str_contains($arn, $this->snsClient->getRegion())) {
            $arn = \str_replace('sqs:', 'sqs:' . $this->snsClient->getRegion() . ':', $arn);
        }
        $this->subscribeSqs($arn);
    }

    public function register(): string
    {
        $arnParts = explode(':', $this->topicArn);
        $response = $this->snsClient->createTopic(['Name' => end($arnParts)]);

        return (string)$response['TopicArn'];
    }

    /**
     * @return \Generator<int, void, Envelope|null, void>
     * @psalm-suppress MixedReturnTypeCoercion
     */
    public function sending(): \Generator
    {
        $envelope = (yield);
        if ($envelope === null) {
            return;
        }
        $promises = [
            self::PART_ONE => [],
            self::PART_TWO => [],
        ];
        $partCurrent = self::PART_ONE;

        while ($envelope) {
            $promises[$partCurrent][] = $this->publishMessage($envelope);
            /** @psalm-suppress RedundantCondition */
            if ($partCurrent === self::PART_ONE && count($promises[self::PART_ONE]) >= 15) {
                $partCurrent = self::PART_TWO;
                if (count($promises[self::PART_TWO]) >= 15) {
                    Utils::all($promises[self::PART_TWO])->wait();
                    $promises[self::PART_TWO] = [];
                }
            }
            if ($partCurrent === self::PART_TWO && count($promises[self::PART_TWO]) >= 15) {
                $partCurrent = self::PART_ONE;
                if (count($promises[self::PART_ONE]) >= 15) {
                    Utils::all($promises[self::PART_ONE])->wait();
                    $promises[self::PART_ONE] = [];
                }
            }
            $envelope = (yield);
        }

        Utils::all(\array_merge($promises[self::PART_TWO], $promises[self::PART_ONE]))->wait();

        $promises[self::PART_TWO] = [];
        $promises[self::PART_ONE] = [];
    }

    private function publishMessage(Envelope $envelope): PromiseInterface
    {
        $publish = (SnsEnvelope::ofEnvelope($envelope))->toSnsArray($this->topicArn);
        $promise = $this->snsClient->publishAsync($publish)->then(
            null,
            function (\Throwable $error) use ($publish): void {
                $message = json_encode($publish, JSON_THROW_ON_ERROR);
                throw new \RuntimeException(
                    "Could not send messages to the sns.",
                    0,
                    new \Exception(
                        "Message: $message",
                        0,
                        $error
                    )
                );
            }
        );

        Utils::queue()->run();

        return $promise;
    }


    private function subscribeSqs(string $sqsArn): void
    {
        $this->subscribe('sqs', $sqsArn);
    }

    private function subscribe(string $protocol, string $endpoint): ?string
    {
        $filterEvents = array_keys($this->messageNameMapIn);
        $subscriptionArn = $this->subscriptionArn($endpoint);

        if (null !== $subscriptionArn && 0 === count($filterEvents)) {
            $this->unsubscribe($subscriptionArn);

            return null;
        }

        if (null === $subscriptionArn && 0 === count($filterEvents)) {
            return null;
        }

        if (null === $subscriptionArn) {
            $subscriptionResult = $this->snsClient->subscribe(
                [
                    'Protocol' => $protocol,
                    'Endpoint' => $endpoint,
                    'ReturnSubscriptionArn' => true,
                    'TopicArn' => $this->topicArn,
                ]
            );
            /** @var string $subscriptionArn */
            $subscriptionArn = $subscriptionResult->get('SubscriptionArn');
            sleep(1);
            $this->checkSubscriptionApprove();
        }

        $this->snsClient->setSubscriptionAttributes(
            [
                'SubscriptionArn' => $subscriptionArn,
                'AttributeName' => 'FilterPolicy',
                'AttributeValue' => json_encode(
                    ['name' => $filterEvents, 'headers' => [['exists' => true]]],
                    JSON_THROW_ON_ERROR
                ),
            ]
        );

        return $subscriptionArn;
    }

    /**
     * array(5) {
     *  ["MessageId"]=>
     *  string(36) "227b0f27-68c9-4c95-b497-4bc86ce6e380"
     *  ["ReceiptHandle"]=>
     *  string(412)
     *  "AQEByQt/S+ssNpIkmJgefOWEiltH4WmZQapelZQk/6m9L5/Xa8BEQ3eh+2zj9Umu3Jraht4BFxaZrNCiCWNv+nEIfKaROa+uUC4i/VvFM0FvB8OQ0ireQUeYEUVolWM/6PmwatP4Q1H4mIurZgkabuV/qVnYv4tK9u+GdN5XsuFHcJdfJPNQoPxSKyeWoww3hDlovV6LZ9QAM3VhZ7jDzDCJ6SQ+x3Csq4ge5SGSB0py+jaxrFzW9Ai2O5V6CgiAwhSqM1S/0nUEoEMvemhHq/isLMbWDd+pzfH3afp+K3/5OT+N41SS9HqqvSgurZkPPFnzSCJfPeZ8NyJ9sx4x/Uu7Wy0w4UVdQYvGw+2uHG3qNzH9OKi8K4r4qCOMTdbsiMquO3dF0hNRAmBvW3sCd66sdw=="
     *  ["MD5OfBody"]=>
     *  string(32) "c441da8b73420307b4d5d3ea74fa06b1"
     *  ["Body"]=>
     *  string(1602) "{
     *  "Type" : "SubscriptionConfirmation",
     *  "MessageId" : "5715668c-4b9a-4ef8-bd84-d87c8769339a",
     *  "Token" :
     *  "2336412f37fb687f5d51e6e2425dacbbaeeb16318e701b12597c0c9335ceb21699f6cff6da6620978cb5e71236e11030a112ee4747e598a1f65510ab1add1ed2564da1f2d75e84437faafd8a3904558c5ed0a4cfe9a62e180c9c4340f6c2f3368171f29b2025274f0b3ab762cb3144eff8487e20e58e5b1af3c33b5e8582dfd5",
     *  "TopicArn" : "arn:aws:sns:eu-central-1:968841124411:services_bus",
     *  "Message" : "You have chosen to subscribe to the topic arn:aws:sns:eu-central-1:968841124411:services_bus.\nTo
     *  confirm the subscription, visit the SubscribeURL included in this message.",
     *  "SubscribeURL" :
     *  "https://sns.eu-central-1.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:eu-central-1:968841124411:services_bus&Token=2336412f37fb687f5d51e6e2425dacbbaeeb16318e701b12597c0c9335ceb21699f6cff6da6620978cb5e71236e11030a112ee4747e598a1f65510ab1add1ed2564da1f2d75e84437faafd8a3904558c5ed0a4cfe9a62e180c9c4340f6c2f3368171f29b2025274f0b3ab762cb3144eff8487e20e58e5b1af3c33b5e8582dfd5",
     *  "Timestamp" : "2022-01-03T12:47:54.139Z",
     *  "SignatureVersion" : "1",
     *  "Signature" :
     *  "sj4LD/09vwG264q9Od/RzjtmTfT6OZ5I73FNpUMy4sKtYBOHJbehXW6ctgEJ8FM5NEkgFvTIN+UmH/gi/ViwXPJCB62Tyll4AvfJmBEyo7DUEfD644nonJi8A/CC1EJj727DHjbj+AO4v/M17XHlV6nhJrxvsq/qkn75VimNELedvf4u3TsE0yAhoozBgIt+7+Waopk9sfJ82cHSDnFLX5LYf8deSEzQI/PObEFxf4Y5k79EMchtr4N+Ppa1pQjDdIxZ5vhqzgDdUVfJ17tz7/qvBHH4N3re0aGvdjL9Q+UfTmzB6ab15cxSU5KUSk2H6nKBtQLtqsmAFoHKZ4dXaQ==",
     *  "SigningCertURL" :
     *  "https://sns.eu-central-1.amazonaws.com/SimpleNotificationService-7ff5318490ec183fbaddaa2a969abfda.pem"
     *}"
     *  ["Attributes"]=>
     *  array(2) {
     *    ["SentTimestamp"]=>
     *    string(13) "1641214074166"
     *    ["ApproximateReceiveCount"]=>
     *    string(1) "1"
     *  }
     *}
     */
    private function checkSubscriptionApprove(): void
    {
        $envelopesGenerator = $this->sqsTransport->receiveRaw(1000);
        while ($envelopesGenerator->valid()) {
            $messageRaw = $envelopesGenerator->current();
            assert(isset($messageRaw['Body']));
            /** @var array{Type:string, Token: string, TopicArn:string} $message */
            $message     = json_decode($messageRaw['Body'], true, 512, JSON_THROW_ON_ERROR);
            $messageType = $message['Type'] ?? '';
            if ($messageType !== 'SubscriptionConfirmation') {
                $envelopesGenerator->send(new Result\Err(new \Exception()));
                continue;
            }

            $this->snsClient->confirmSubscription(
                [
                    'Token'    => $message['Token'],
                    'TopicArn' => $message['TopicArn'],
                ]
            );

            $envelopesGenerator->send(new Result\Ok(null));
        }
    }

    private function subscriptionArn(string $endpoint): ?string
    {
        $params = ['TopicArn' => $this->topicArn,];
        while (true) {
            /** @var array{NextToken:string, Subscriptions:array<array{Endpoint: string, SubscriptionArn:string}>} $result */
            $result = $this->snsClient->listSubscriptionsByTopic($params);

            foreach ($result['Subscriptions'] ?? [] as $subscription) {
                if ($endpoint === $subscription['Endpoint']) {
                    return $subscription['SubscriptionArn'];
                }
            }

            if (isset($result['NextToken'])) {
                $params['NextToken'] = $result['NextToken'];
            } else {
                return null;
            }
        }

        return null;
    }

    private function unsubscribe(string $subscriptionArn): void
    {
        $this->snsClient->unsubscribe(
            [
                'SubscriptionArn' => $subscriptionArn,
            ]
        );
    }
}

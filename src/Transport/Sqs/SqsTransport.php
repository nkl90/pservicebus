<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Sqs;

use Aws\Credentials\CredentialProvider;
use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;
use Enqueue\Dsn\Dsn;
use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBus\Transport\Transport;
use GDXbsv\PServiceBus\Transport\TransportSynchronisation;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\Utils;
use Prewk\Result;

class SqsTransport implements TransportSynchronisation, Transport
{
    private const THROUGH_PUT_MAX_LIMIT = 10;

    private const PART_ONE = 0;
    private const PART_TWO = 1;
    private const SIZE_LIMIT_BYTES = 100000;
    private const ACCUMULATE = 30;
    private string $sqsEndpoint;

    private string $queueNameDlq;
    private bool $shouldStop = false;

    private ?string $allowFrom = null;

    /**
     * @param array<string,string> $tags
     */
    public function __construct(
        private string $queueName,
        private SqsClient $sqsClient,
        private string $sqsNamespace,
        private int $retries,
        private array $tags = [],
    ) {
        $this->sqsEndpoint = (string)$sqsClient->getEndpoint();
        $this->queueNameDlq = $queueName . '_DL';
    }

    public static function ofDsn(
        string $dsnString,
        array $clientAdditionalConfig = [],
    ): self {
        $dsn = Dsn::parseFirst($dsnString);
        if (null === $dsn || $dsn->getSchemeProtocol() !== 'sqs') {
            throw new \InvalidArgumentException(
                'Malformed parameter "dsn". example: "sqs+http://key:secret@aws:4100/123456789012?region=eu-west-1&retries=3&tags[name]=value&tags[name2]=value2&queue=QueueName"'
            );
        }

        $namespace = $dsn->getPath();
        assert(
            $namespace !== null,
            'error(-><-): sqs+http://key:secret@aws:4100->/123456789012<-?region=eu-west-1&queue=QueueName'
        );
        $queue = $dsn->getString('queue');
        assert(
            $queue !== null,
            'error(-><-): sqs+http://key:secret@aws:4100/123456789012?region=eu-west-1&->queue=QueueName<-'
        );
        $region = $dsn->getString('region');
        assert(
            $region !== null,
            'error(-><-): sqs+http://key:secret@aws:4100/123456789012?->region=eu-west-1<-&queue=QueueName'
        );
        $retries = ($dsn->getDecimal('retries') ?? 3);
        /** @var array<string,string> $tags */
        $tags = $dsn->getArray('tags')->toArray();
        $user = $dsn->getUser();
        $password = $dsn->getPassword();

        $sqsClientConfig = [
            'version' => 'latest',
            'region' => $region,
        ];
        if ($user !== null && $password !== null) {
            $sqsClientConfig['credentials'] = [
                'key' => $user,
                'secret' => $password,
            ];
        } else {
            $provider = CredentialProvider::defaultProvider();
            $sqsClientConfig['credentials'] = $provider;
        }
        $host = $dsn->getHost();
        if ($host !== null) {
            /**
             * @psalm-suppress PossiblyUndefinedIntArrayOffset
             * @var string $schemeExtension
             */
            $schemeExtension = $dsn->getSchemeExtensions()[0];
            $endpoint = sprintf(
                '%s://%s',
                $schemeExtension,
                $host
            );
            if ($dsn->getPort() !== null) {
                $endpoint .= ":{$dsn->getPort()}";
            }
            $sqsClientConfig['endpoint'] = $endpoint;
        }

        return new self(
            $queue,
            new SqsClient(array_merge($sqsClientConfig, $clientAdditionalConfig)),
            trim($namespace, '/'),
            $retries,
            $tags
        );
    }

    public function setAllowFrom(?string $allowFrom): void
    {
        $this->allowFrom = $allowFrom;
    }

    public function arn(): string
    {
        return $this
            ->sqsClient
            ->getQueueArn($this->genUrl());
    }

    /**
     * @return \Generator<int, void, Envelope|null, void>
     * @psalm-suppress MixedReturnTypeCoercion
     */
    public function sending(): \Generator
    {
        $envelope = (yield);
        if ($envelope === null) {
            return;
        }
        $batchList = [];
        $promises = [
            self::PART_ONE => [],
            self::PART_TWO => [],
        ];
        $partCurrent = self::PART_ONE;
        while ($envelope) {
            $batchList[] = SqsEnvelope::ofEnvelope($envelope)->toSqsArray();
            if (self::THROUGH_PUT_MAX_LIMIT > \count($batchList)) {
                $envelope = (yield);
                continue;
            }
            if (self::SIZE_LIMIT_BYTES < strlen(json_encode($batchList))) {
                foreach ($batchList as $message) {
                    $promises[$partCurrent][] = $this->sendMessagesBatch([$message]);
                }
            } else {
                $promises[$partCurrent][] = $this->sendMessagesBatch($batchList);
            }
            $batchList = [];
            /** @psalm-suppress RedundantCondition */
            if ($partCurrent === self::PART_ONE && count($promises[self::PART_ONE]) >= self::ACCUMULATE) {
                $partCurrent = self::PART_TWO;
                if (count($promises[self::PART_TWO]) >= self::ACCUMULATE) {
                    Utils::all($promises[self::PART_TWO])->wait();
                    $promises[self::PART_TWO] = [];
                }
            }
            if ($partCurrent === self::PART_TWO && count($promises[self::PART_TWO]) >= self::ACCUMULATE) {
                $partCurrent = self::PART_ONE;
                if (count($promises[self::PART_ONE]) >= self::ACCUMULATE) {
                    Utils::all($promises[self::PART_ONE])->wait();
                    $promises[self::PART_ONE] = [];
                }
            }
            $envelope = (yield);
        }

        if (0 !== count($batchList)) {
            $promises[$partCurrent][] = $this->sendMessagesBatch($batchList);
        }

        Utils::all(\array_merge($promises[self::PART_TWO], $promises[self::PART_ONE]))->wait();

        $promises[self::PART_TWO] = [];
        $promises[self::PART_ONE] = [];
    }

    /**
     * @return \Generator<int, Envelope, Result\Ok<null, mixed>|Result\Err<mixed, \Exception>, void>
     */
    public function receive(int $limit = 0): \Generator
    {
        $generator = $this->receiveRaw($limit);
        while ($generator->valid()) {
            /** @psalm-suppress PossiblyNullArgument according to type it not null */
            $result = (yield SqsEnvelope::ofSqs($generator->current())->toEnvelope());
            $generator->send($result);
        }
    }

    /**
     * @return \Generator<int, array{Body:string, MessageAttributes:array<string,array{StringValue: string, DataType: string}>}, Result\Ok<null, mixed>|Result\Err<mixed, \Exception>, void>
     */
    public function receiveRaw(int $limit = 0): \Generator
    {
        $messagesLeft = $limit;
        // if limit = 0, then reset to 10 (self::MAXLIMIT)
        // if $messagesLeft is 5, it will take 5. If it’s 100, then it will be 10 (self::MAXLIMIT).
        $messagesToProcessFind = function (int $messagesLeft) use ($limit): int {
            return ($limit === 0)
                ? self::THROUGH_PUT_MAX_LIMIT
                : min(
                    $messagesLeft,
                    self::THROUGH_PUT_MAX_LIMIT
                );
        };
        $messagesToProcess = $messagesToProcessFind($messagesLeft);
        $messagesPromise = $this->receiveMessages($messagesToProcess);
        while (true) {
            $messages = $messagesPromise->wait();
            $messagesLeft = ($messagesLeft === 0) ? $messagesLeft : $messagesLeft - count($messages);
            if ($limit === 0 && !$this->shouldStop && count($messages) === 0) {
                $this->waitForContinue(40);
            }
            if (($limit === 0 && !$this->shouldStop) || ($messagesLeft > 0 && count($messages) > 0 && count(
                        $messages
                    ) === self::THROUGH_PUT_MAX_LIMIT && !$this->shouldStop)) {
                $messagesToProcess = $messagesToProcessFind($messagesLeft);
                $messagesPromise = $this->receiveMessages($messagesToProcess);
            } else {
                $messagesPromise = null;
            }

            foreach ($messages as $message) {
                /** @var Result $isSucceed */
                $isSucceed = (yield $message);

                if ($isSucceed->isErr()) {
                    $this->extendVisibilityTimeout($message);
                    continue;
                }
                $this->sqsClient->deleteMessage(
                    [
                        'QueueUrl' => $this->genUrl(),
                        'ReceiptHandle' => $message['ReceiptHandle'],
                    ]
                );
            }
            if ($messagesPromise === null) {
                break;
            }
        }
    }

    public function stop(): void
    {
        $this->shouldStop = true;
    }

    public function sync(): void
    {
        $result = $this->sqsClient->listQueues(
            [
                'QueueNamePrefix' => $this->queueName,
            ]
        );
        $checkNameInUrl = '/' . $this->queueName;
        /** @var array<string>|null $queueUrls */
        $queueUrls = $result->get('QueueUrls');
        if (is_array($queueUrls)) {
            foreach ($queueUrls as $queueUrl) {
                if (str_ends_with($queueUrl, $checkNameInUrl)) {
                    return;
                }
            }
        }

        // create dead letter queue
        /** @var array{QueueUrl: string} $deadLetterCreateResult */
        $deadLetterCreateResult = $this
            ->sqsClient
            ->createQueue(['QueueName' => $this->queueNameDlq, 'tags' => $this->tags]);

        $dlqArn = $this->sqsClient->getQueueArn($deadLetterCreateResult['QueueUrl']);
        // mostly need for tests (gtilab-ci problems)
        $parts = explode(':', $dlqArn);
        $parts[2] = 'sqs';
        $dlqArn = implode(':', $parts);
        if (!str_contains($dlqArn, $this->sqsClient->getRegion())) {
            $dlqArn = \str_replace('sqs:', 'sqs:' . $this->sqsClient->getRegion() . ':', $dlqArn);
        }

        $config = [
            'QueueName' => $this->queueName,
            'Attributes' => [
                'RedrivePolicy' => json_encode(
                    [
                        'deadLetterTargetArn' => $dlqArn,
                        'maxReceiveCount' => $this->retries,
                    ],
                    JSON_THROW_ON_ERROR
                ),
            ],
            'tags' => $this->tags,
        ];

        if ($this->allowFrom !== null) {
            $config['Attributes']['Policy'] = json_encode(
                [
                    'Version' => '2012-10-17',
                    'Id' => $this->arn() . '/SQSDefaultPolicy',
                    'Statement' =>
                        [
                            [
                                'Sid' => 'Sid' . (string)\random_int(0, 9999999999999),
                                'Effect' => 'Allow',
                                'Principal' => '*',
                                'Action' => 'SQS:SendMessage',
                                'Resource' => $this->arn(),
                                'Condition' =>
                                    [
                                        'ArnEquals' =>
                                            [
                                                'aws:SourceArn' => $this->allowFrom,
                                            ],
                                    ],
                            ],
                        ],
                ],
                JSON_THROW_ON_ERROR
            );
        }


        $this->sqsClient->createQueue($config);
    }

    private function sendMessagesBatch(array $entries, int $tryTo = 3): PromiseInterface
    {
        $sqsMessages = [
            'Entries' => $entries,
            'QueueUrl' => $this->genUrl(),
        ];

        $promise = $this->sqsClient->sendMessageBatchAsync($sqsMessages)->then(
            function (\Aws\Result $result) use ($entries, $tryTo): void {
                /** @var array<array{Code: string, Id: string, Message: string, SenderFault: string}> $failedQueues */
                $failedQueues = $result->toArray()['Failed'] ?? [];
                if (!empty($failedQueues)) {
                    if ($tryTo > 0) {
                        --$tryTo;
                        \usleep(\random_int(100000, 1000000)); //100ms - 1s
                        $this->sendMessagesBatch($entries, $tryTo)->wait();
                    } else {
                        $failedQueuesJson = json_encode($failedQueues, JSON_THROW_ON_ERROR);
                        $messagesForBatchJson = json_encode($entries, JSON_THROW_ON_ERROR);
                        throw new \RuntimeException(
                            "Could not send message to Queue during batch send queues inside Sqs/SendCoroutine.",
                            0,
                            new \Exception(
                                "FailedQueues: $failedQueuesJson",
                                0,
                                new \Exception("messagesForBatch: $messagesForBatchJson")
                            )
                        );
                    }
                }
            },
            function (\Throwable $error) use ($sqsMessages): void {
                $sqsMessagesJson = json_encode($sqsMessages, JSON_THROW_ON_ERROR);

                throw new \RuntimeException(
                    "Could not send messages to the queue.",
                    0,
                    new \Exception(
                        "Messages: $sqsMessagesJson",
                        0,
                        $error
                    )
                );
            }
        );

        Utils::queue()->run();

        return $promise;
    }

    /**
     * @return PromiseInterface<array<array{Attributes: array<string, string|int>, ReceiptHandle: string, Body: string,
     *   MessageId:string, MessageAttributes:array<string,array{StringValue: string, DataType: string}>}>>
     */
    private function receiveMessages(int $limit = self::THROUGH_PUT_MAX_LIMIT): PromiseInterface
    {
        /**
         * @var PromiseInterface<array<array{Attributes: array<string, string|int>, ReceiptHandle: string, Body: string, MessageId:string, MessageAttributes:array<string,array{StringValue: string, DataType: string}>}>> $messagePromise
         * @var array{Messages?: array<array{Attributes: array<string, string|int>, ReceiptHandle: string, Body: string, MessageId: string, MessageAttributes:array<string,array{StringValue: string, DataType: string}>}>} $result
         */
        $messagePromise = $this->sqsClient->receiveMessageAsync(
            [
                'AttributeNames' => ['SentTimestamp', 'ApproximateReceiveCount'],
                'MaxNumberOfMessages' => $limit,
                'MessageAttributeNames' => ['All'],
                'QueueUrl' => $this->genUrl(),
                'WaitTimeSeconds' => 20,
            ]
        )->then(
            function (\Aws\Result $result) {
                return $result['Messages'] ?? [];
            }
        );
        Utils::queue()->run();

        return $messagePromise;
    }

    /**
     * @param array{Attributes: array<string, string|int> , ReceiptHandle: string} $message
     */
    private function extendVisibilityTimeout(array $message): void
    {
        $retryNumber = (int)($message['Attributes']['ApproximateReceiveCount'] ?? 0);
        $receiptHandle = $message['ReceiptHandle'];

        if ($retryNumber < 1) {
            return;
        }
        $visibilityTimeout = 60 * (2 ** $retryNumber); //seconds

        try {
            $this->sqsClient->changeMessageVisibility(
                [
                    'QueueUrl' => $this->genUrl(),
                    'ReceiptHandle' => $receiptHandle,
                    'VisibilityTimeout' => $visibilityTimeout,
                ]
            );
        } catch (SqsException $e) {
            if ('InvalidParameterValue' === $e->getAwsErrorCode()) {
                //skip this case. We do not really care. Iy happens time to time.
                return;
            }

            throw $e;
        }
    }

    private function genUrl(): string
    {
        return sprintf(
            '%s/%s/%s',
            $this->sqsEndpoint,
            $this->sqsNamespace,
            $this->queueName
        );
    }

    private function waitForContinue(int $seconds): void
    {
        for ($i = 0; $i < $seconds; ++$i) {
            sleep(1);
            if ($this->shouldStop) {
                return;
            }
        }
    }
}

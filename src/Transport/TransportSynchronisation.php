<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport;

interface TransportSynchronisation
{
    public function sync(): void;
}

<?php

declare(strict_types=1);

namespace Integration\Handling;

use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBusTests\Integration\IntegrationTestCase;
use JetBrains\PhpStorm\Immutable;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
#[Immutable]
final class ErrorTest extends IntegrationTestCase
{
    public function testBrokenMessages()
    {
        $inMemmory = $this->inMemTransport;
        $sending = $inMemmory->sending();
        self::expectExceptionMessage('Consume: Undefined array key "name"');
        $sending->send(new Envelope([], 0, 0, []));
        $sending->send(null);
    }
}

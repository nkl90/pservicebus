<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration\Saga;

use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBusTestApp\Saga\CustomDoctrineSearchEvent;
use GDXbsv\PServiceBusTestApp\Saga\TestSaga;
use GDXbsv\PServiceBusTestApp\Saga\TestSagaCommand;
use GDXbsv\PServiceBusTestApp\Saga\TestSagaCreateCommand;
use GDXbsv\PServiceBusTestApp\Saga\TestSagaMapStringCommand;
use GDXbsv\PServiceBusTestApp\Saga\TestSagaRemoveCommand;
use GDXbsv\PServiceBusTestApp\Saga\TestsSagaInEvent;
use GDXbsv\PServiceBusTestApp\Saga\TestsSagaOutputEvent;
use GDXbsv\PServiceBusTests\Integration\DoctrineIntegrationTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class SagaDoctrineTest extends DoctrineIntegrationTestCase
{
    public function testSaving()
    {
        $bus = $this->bus;
        $bus->trace();
        $bus->send($commandCreate = new TestSagaCreateCommand());
        $this->em->clear();
        /** @var ?TestSaga $saga */
        $saga = $this->em->getRepository(TestSaga::class)->find(new Id($commandCreate->id));
        self::assertNotNull($saga);
        self::assertEquals($commandCreate->string, $saga->string);
    }
    public function testRemoving()
    {
        $bus = $this->bus;
        $bus->trace();
        $bus->send($commandCreate = new TestSagaCreateCommand());
        $this->em->clear();
        /** @var ?TestSaga $saga */
        $saga = $this->em->getRepository(TestSaga::class)->find(new Id($commandCreate->id));
        self::assertNotNull($saga);
        self::assertEquals($commandCreate->string, $saga->string);

        //remove
        $this->em->clear();
        $bus->send($commandRemove = new TestSagaRemoveCommand());

        $this->em->clear();
        /** @var ?TestSaga $saga */
        $saga = $this->em->getRepository(TestSaga::class)->find(new Id($commandCreate->id));
        self::assertNull($saga);

    }

    public function testRetriveCustomSaga()
    {
        $bus = $this->bus;
        $commandCreate = new TestSagaCreateCommand();
        $commandCreate->string = 'customSearchString';
        $bus->send($commandCreate);
        $this->em->clear();
        $event = new CustomDoctrineSearchEvent();
        $event->string = 'customSearchString';
        $bus->trace();
        $bus->publish($event);
        self::assertEquals(
            'customSearchString',
            $bus->getEventMessageByClass(TestsSagaOutputEvent::class)->payload->result
        );
    }

    public function testMappingNotId()
    {
        $bus = $this->bus;
        $commandCreate = new TestSagaCreateCommand();
        $commandCreate->string = 'mappingToString';
        $bus->send($commandCreate);
        $this->em->clear();
        $command2 = new TestSagaMapStringCommand();
        $command2->string = 'mappingToString';
        $bus->trace();
        $bus->send($command2);
        self::assertEquals(
            $commandCreate->id,
            $bus->getEventMessageByClass(TestsSagaOutputEvent::class)->payload->result
        );
    }

    public function testSendAllMessages()
    {
        $bus = $this->bus;
        $commandCreate = new TestSagaCreateCommand();
        $bus->send($commandCreate);
        $this->saveMessagesInTable([new Message(new TestsSagaInEvent(), EventOptions::record())]);
        $bus->trace();
        $this->sendAllMessages();
        self::assertEquals(
            'testListeningFunction',
            $bus->getEventMessageByClass(TestsSagaOutputEvent::class)->payload->result
        );
    }
}

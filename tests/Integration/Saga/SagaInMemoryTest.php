<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration\Saga;

use GDXbsv\PServiceBusTestApp\Saga\CustomSearchEvent;
use GDXbsv\PServiceBusTestApp\Saga\TestSagaCommand;
use GDXbsv\PServiceBusTestApp\Saga\TestSagaCreateCommand;
use GDXbsv\PServiceBusTestApp\Saga\TestsSagaInEvent;
use GDXbsv\PServiceBusTestApp\Saga\TestsSagaOutputEvent;
use GDXbsv\PServiceBusTests\Integration\IntegrationTestCase;

class SagaInMemoryTest extends IntegrationTestCase
{
    public function testCommand()
    {
        $bus = $this->bus;
        $bus->send($commandCreate = new TestSagaCreateCommand());
        $bus->trace();
        $bus->send($command = new TestSagaCommand());
        self::assertEquals('testHandlerFunction', $bus->getEventMessages()[0]->payload->result);
    }

    public function testEvents()
    {
        $bus = $this->bus;
        $bus->send($commandCreate = new TestSagaCreateCommand());
        $bus->trace();
        $bus->publish(new TestsSagaInEvent());
        self::assertEquals(
            'testListeningFunction',
            $bus->getEventMessageByClass(TestsSagaOutputEvent::class)->payload->result
        );
    }

    public function testRetriveCustomSaga()
    {
        $bus = $this->bus;
        $bus->send($commandCreate = new TestSagaCreateCommand());
        $bus->trace();
        $bus->publish(new CustomSearchEvent());
        self::assertEquals(
            'customSagaId',
            $bus->getEventMessageByClass(TestsSagaOutputEvent::class)->payload->result
        );
    }

//    public function testTimeout()
//    {
//        /** @var TraceableEventBus $traceEventBus */
//        $traceEventBus = self::$container->get(EventBus::class);
//        $traceEventBus->trace();
//        $traceEventBus->publish(new DomainEventStream([createDomainMessage(new TestsSagaInTimeoutEvent(), '')]));
//        $this->consume('saga');
//        $events = $traceEventBus->getEvents();
//        self::assertCount(2, $events);
//        sleep(6);
//        $this->consume('saga');
//        $events = $traceEventBus->getEvents();
//        self::assertCount(3, $events);
//        self::assertEquals('testListeningAfterTimeoutFunction', $events[2]->result);
//    }

//    protected function setUp(): void
//    {
//        parent::setUp();
//        $this->createSagasTables();
//    }
//
//    private function createSagasTables()
//    {
//        $tool    = new \Doctrine\ORM\Tools\SchemaTool($this->em);
//        $classes = [
//            $this->em->getClassMetadata(TestSaga::class),
//        ];
//        $tool->updateSchema($classes);
//    }
}
